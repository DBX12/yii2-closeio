<?php

namespace dbx12\closeio;

use LooplineSystems\CloseIoApiWrapper\Api\ActivityApi;
use LooplineSystems\CloseIoApiWrapper\Api\ContactApi;
use LooplineSystems\CloseIoApiWrapper\Api\CustomFieldApi;
use LooplineSystems\CloseIoApiWrapper\Api\LeadApi;
use LooplineSystems\CloseIoApiWrapper\Api\LeadStatusApi;
use LooplineSystems\CloseIoApiWrapper\Api\OpportunityApi;
use LooplineSystems\CloseIoApiWrapper\Api\OpportunityStatusApi;
use LooplineSystems\CloseIoApiWrapper\Api\TaskApi;
use LooplineSystems\CloseIoApiWrapper\Api\UserApi;
use LooplineSystems\CloseIoApiWrapper\CloseIoApiWrapper;
use LooplineSystems\CloseIoApiWrapper\CloseIoConfig;
use yii\base\Component;

/**
 * Class CloseComponent
 *
 * @package dbx12\closeio
 * @property LeadApi              leadApi
 * @property OpportunityApi       opportunityApi
 * @property LeadStatusApi        leadStatusApi
 * @property OpportunityStatusApi opportunityStatusApi
 * @property CustomFieldApi       customFieldApi
 * @property ContactApi           contactApi
 * @property ActivityApi          activityApi
 * @property TaskApi              taskApi
 * @property UserApi              userApi
 */
class CloseComponent extends Component
{
    /** @var string */
    public $apiKey;

    /** @var CloseIoApiWrapper */
    protected $apiWrapper;

    public function init()
    {
        parent::init();
        $config = new CloseIoConfig();
        $config->setApiKey($this->apiKey);
        /** @noinspection PhpUnhandledExceptionInspection exception is thrown on unset apiKey or url */
        $this->apiWrapper = new CloseIoApiWrapper($config);
    }

    public function getLeadApi(): LeadApi
    {
        return $this->apiWrapper->getLeadApi();
    }

    public function getOpportunityApi(): OpportunityApi
    {
        return $this->apiWrapper->getOpportunityApi();
    }

    public function getLeadStatusApi(): LeadStatusApi
    {
        return $this->apiWrapper->getLeadStatusesApi();
    }

    public function getOpportunityStatusApi(): OpportunityStatusApi
    {
        return $this->apiWrapper->getOpportunityStatusesApi();
    }

    public function getCustomFieldApi(): CustomFieldApi
    {
        return $this->apiWrapper->getCustomFieldApi();
    }

    public function getContactApi(): ContactApi
    {
        return $this->apiWrapper->getContactApi();
    }

    public function getActivityApi(): ActivityApi
    {
        return $this->apiWrapper->getActivitiesApi();
    }

    public function getTaskApi(): TaskApi
    {
        return $this->apiWrapper->getTaskApi();
    }

    public function getUserApi(): UserApi
    {
        return $this->apiWrapper->getUserApi();
    }


}
