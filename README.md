Yii2 Close api component
======================
Connect you Yii2 application with the Close CRM (previously Close.io) API. This project wraps
[loopline-systems/closeio-api-wrapper](https://github.com/loopline-systems/closeio-api-wrapper) in a Yii2 component.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist dbx12/yii2-closeio "*"
```

or add

```
"dbx12/yii2-closeio": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, configure it as component in your config files. You will need an API key for your
close organization. Read more about API keys and how to get them in the close documentation
[here](https://help.close.com/docs/api-keys).

```php
return [
    'components' => [
        'close' => [
            'class' => \dbx12\closeio\CloseComponent::class,
            'apiKey' => 'api-key-here'
        ],
    ],
];
```

You can than use it as component, for example, if you want to find the lead `lead_aaaaaaaabbbbbbbcccccccdddddddeeeeeeefffffff`
you could use this snippet to get a `LooplineSystems\CloseIoApiWrapper\Model\Lead` object.

```php
$shipment = Yii::$app->close->leadApi->getLead('lead_aaaaaaaabbbbbbbcccccccdddddddeeeeeeefffffff');
```

You can read how to use the different APIs in the documentation of the actual API project
[here](https://github.com/loopline-systems/closeio-api-wrapper)

